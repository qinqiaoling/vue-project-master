// 导入组件
import Vue from 'vue';
import Router from 'vue-router';
// 登录
import login from '@/views/login';
// 首页
import index from '@/views/index';
/**
 * 基础菜单
 */
// 商品管理
import Goods from '@/views/goods/Goods';
// 机器信息管理
import Machine from '@/views/machine/Machine';
// 货道信息管理
import MachineAisle from '@/views/machine/MachineAisle';
/**
 * 订单管理
 */
// 交易订单
import Order from '@/views/pay/Order';
/**
 * 系统管理
 */
// 用户管理
import user from '@/views/system/user';
// 菜单管理
import Module from '@/views/system/Module';
// 角色管理
import Role from '@/views/system/Role';
// 公司管理
import Dept from '@/views/system/Dept';
// 系统环境变量
import Variable from '@/views/system/Variable';
// 权限管理
import Permission from '@/views/system/Permission';
/**
 * 支付管理
 */
// 支付配置信息
import MachineConfig from '@/views/machine/MachineConfig';
// 支付配置
import Config from '@/views/pay/Config';
/**
 * 数据监控
 */
// 监控查询
import druidLogin from '@/views/druid/login';

// 图表界面
import statistics from '@/views/charts/statistics';

// 应急调度
import emergency from '@/views/emergency/index';
// 视频调度
import videoDispatch from '@/views/emergency/video';
// 音频调度
import audioDispatch from '@/views/emergency/audio';
// GIS调度
import GISDispatch from '@/views/emergency/GIS';

// GIS调度-gisMarket
import gisMarket from '@/views/emergency/gisMarket';

// 流媒体
import liveplayer from '@/views/emergency/liveplayer'

//立体柱形
import barecharts from '@/views/emergency/barecharts'

//立体饼图
import pieecharts from '@/views/emergency/pieecharts'

// chat-ai封装请求
import chatRequest from '@/views/emergency/chatRequest'

// 启用路由
Vue.use(Router);

// 导出路由 
export default new Router({
    routes: [{
        path: '/',
        name: '',
        component: login,
        hidden: true,
        meta: {
            requireAuth: false
        }
    }, {
        path: '/login',
        name: '登录',
        component: login,
        hidden: true,
        meta: {
            requireAuth: false
        }
    }, {
        path: '/index',
        name: '首页',
        component: index,
        iconCls: 'el-icon-tickets',
        children: [
            // {
            //     path: '/goods/Goods',
            //     name: '商品管理',
            //     component: Goods,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/machine/Machine',
            //     name: '机器信息管理',
            //     component: Machine,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/machine/MachineAisle',
            //     name: '货道信息管理',
            //     component: MachineAisle,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/pay/Order',
            //     name: '交易订单',
            //     component: Order,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/system/user',
            //     name: '用户管理',
            //     component: user,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/system/Module',
            //     name: '菜单管理',
            //     component: Module,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/system/Role',
            //     name: '角色管理',
            //     component: Role,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/system/Dept',
            //     name: '公司管理',
            //     component: Dept,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/system/Variable',
            //     name: '系统环境变量',
            //     component: Variable,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/system/Permission',
            //     name: '权限管理',
            //     component: Permission,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/machine/MachineConfig',
            //     name: '支付配置信息',
            //     component: MachineConfig,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/pay/Config',
            //     name: '支付配置',
            //     component: Config,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/druid/login',
            //     name: '监控查询',
            //     component: druidLogin,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            // {
            //     path: '/charts/statistics',
            //     name: '数据可视化',
            //     component: statistics,
            //     meta: {
            //         requireAuth: true
            //     }
            // },
            {
                path: '/emergency/index',
                name: '应急调度',
                component: emergency,
                meta: {
                    requireAuth: true
                }
            },
            {
                path: '/emergency/video',
                name: '视频调度',
                component: videoDispatch,
                meta: {
                    requireAuth: true
                }
            },
            {
                path: '/emergency/audio',
                name: '音频调度',
                component: audioDispatch,
                meta: {
                    requireAuth: true
                }
            },
            {
                path: '/emergency/gis',
                name: 'GIS',
                component: GISDispatch,
                meta: {
                    requireAuth: true
                }
            },
            {
                path: '/emergency/gismarket',
                name: 'gismarket',
                component: gisMarket,
                meta: {
                    requireAuth: true
                }
            },
            {
                path: '/emergency/liveplayer',
                name: 'liveplayer',
                component: liveplayer,
                meta: {
                    requireAuth: true
                }
            },
            {
                path: '/emergency/barecharts',
                name: 'barecharts',
                component: barecharts,
                meta: {
                    requireAuth: true
                }
            },
            {
                path: '/emergency/pieecharts',
                name: 'pieecharts',
                component: pieecharts,
                meta: {
                    requireAuth: true
                }
            },
            {
                path: '/emergency/chatRequest',
                name: 'chatRequest',
                component: chatRequest,
                meta: {
                    requireAuth: true
                }
            },
            {
                path: '/emergency/lightgallery',
                name: 'lightgallery',
                component: () => import('@/views/emergency/lightgallery'),
                meta: {
                    requireAuth: true
                }
            },
            {
                path: '/emergency/svgAnimation',
                name: 'svgAnimation',
                component: () => import('@/views/emergency/svg-animation'),
                meta: {
                    requireAuth: true
                }
            }

        ]
    }]
})